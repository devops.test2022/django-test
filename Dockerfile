FROM python:3.6


WORKDIR /app
COPY . /app

EXPOSE 8000
RUN pip install -r /app/requirements.txt
